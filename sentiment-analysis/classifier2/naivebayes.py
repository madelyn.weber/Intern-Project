import os
import sys
import math
from pathlib import Path 
from collections import Counter
from csv import reader
import nltk
import string
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.tokenize.casual import TweetTokenizer


DATA_FILENAME = './sentiment-analysis/data/all_cleaned_reviews.csv'

class NaiveBayes:

    def __init__(self):
        # total number of reviews or "docs" in the dataset
        self.totalDocs = 0
        # number of one-star reviews or "docs" in the dataset
        self.oneNumDocs = 0
        # number of two-star reviews or "docs" etc. in the dataset
        self.twoNumDocs = 0
        self.threeNumDocs = 0
        self.fourNumDocs = 0
        self.fiveNumDocs = 0
        # dictionary containing vocab of entire training set
        self.vocab = {}
        # dictionary of {word1: frequency_1, word2: frequency_1, ...}
        self.oneWordCount = {}
        # dictionary of {word1: frequency_2, word2: frequency_2, ...}
        self.twoWordCount = {}
        # dictionary of {word1: frequency_3, word2: frequency_3, ...}
        self.threeWordCount = {}
        # dictionary of {word1: frequency_4, word2: frequency_4, ...}
        self.fourWordCount = {}
        # dictionary of {word1: frequency_5, word2: frequency_5, ...}
        self.fiveWordCount = {}

    # PREPROCESS FUNCTION  ----------------------------------------------
    def preprocess(self, input_str):
        tknzr = TweetTokenizer()
        # nltk.download('stopwords') # Downloads list of stopwords in your nltk_data folder, only needs to be run once
        # combined list of stopwords and punctuation
        removeList = set(stopwords.words('english') + list(string.punctuation))
        # tokenize and remove stopwords/punctuation, each review treated as one document in the corpus
        text = input_str.lower()
        tokens = tknzr.tokenize(text)
        words = [tok for tok in tokens if not tok in removeList]
        return words

    # FUNCTION THAT EXTRACTS VOCAB OF TRAINING DATA AND OTHER RELATED INFO  ----------------------------------------------
    def getVocab(self, trainingSet):
        #string of text combined from all documents in the ratings categories
        oneStr = ""
        twoStr = ""
        threeStr = ""
        fourStr = ""
        fiveStr = ""

        # iterate through all reviews
        # skip first line i.e. read header first and then iterate over each row in csv as a list
        with open(DATA_FILENAME, 'r') as f:
            # Iterate over each review in the training set to get the number of reviews in each class

            for row in trainingSet:
                rating = row[0]
                if rating == '1':
                    self.oneNumDocs += 1
                    # add doc string to single string containing all docs in the one-star category
                    oneStr += row[1]
                elif rating == '2':
                    self.twoNumDocs += 1
                    # add doc string to single string containing all docs in the two-star category
                    twoStr += row[1]
                elif rating == '3':
                    self.threeNumDocs += 1
                    # add doc string to single string containing all docs in the three-star category
                    threeStr += row[1]
                elif rating == '4':
                    self.fourNumDocs += 1
                    # add doc string to single string containing all docs in the four-star category
                    fourStr += row[1]
                elif rating == '5':
                    self.fiveNumDocs += 1
                    # add doc string to single string containing all docs in the five-star category
                    fiveStr += row[1]
            oneTokens = self.preprocess(oneStr)
            twoTokens = self.preprocess(twoStr)
            threeTokens = self.preprocess(threeStr)
            fourTokens = self.preprocess(fourStr)
            fiveTokens = self.preprocess(fiveStr)

        for tok in oneTokens:
            if tok not in self.vocab:
                self.vocab[tok] = 0 # filler value of 0 for all words in the dictionary
            if tok in self.oneWordCount:
                self.oneWordCount[tok] += 1
            else:
                self.oneWordCount[tok] = 1
        for tok in twoTokens:
            if tok not in self.vocab:
                self.vocab[tok] = 0 # filler value of 0 for all words in the dictionary
            if tok in self.twoWordCount:
                self.twoWordCount[tok] += 1
            else:
                self.twoWordCount[tok] = 1
        for tok in threeTokens:
            if tok not in self.vocab:
                self.vocab[tok] = 0 # filler value of 0 for all words in the dictionary
            if tok in self.threeWordCount:
                self.threeWordCount[tok] += 1
            else:
                self.threeWordCount[tok] = 1
        for tok in fourTokens:
            if tok not in self.vocab:
                self.vocab[tok] = 0 # filler value of 0 for all words in the dictionary
            if tok in self.fourWordCount:
                self.fourWordCount[tok] += 1
            else:
                self.fourWordCount[tok] = 1
        for tok in fiveTokens:
            if tok not in self.vocab:
                self.vocab[tok] = 0 # filler value of 0 for all words in the dictionary
            if tok in self.fiveWordCount:
                self.fiveWordCount[tok] += 1
            else:
                self.fiveWordCount[tok] = 1

    # FUNCTION that trains a Naive Bayes classifier usiing multinomial NB implentation ----------------------------------------------
    # input: list of reviews to be used for training
    # output: data structure with class probabilities (or log of proobabilities)
    # output: data structure with word conditional probabilities (or log of probabilites)
    # output: any other paramters required (e.g., vocab size)
    def trainNaiveBayes(self, trainingSet):
        self.getVocab(trainingSet)
        # CALCULATE PROBABLITIES
        classPs = {}
        wordConditionalPs = {}
        self.totalDocs = len(trainingSet)
        classPs["one"], wordConditionalPs["one"] = calcP(self.totalDocs, self.oneNumDocs, self.vocab, self.oneWordCount)
        classPs["two"], wordConditionalPs["two"] = calcP(self.totalDocs, self.twoNumDocs, self.vocab, self.twoWordCount)
        classPs["three"], wordConditionalPs["three"] = calcP(self.totalDocs, self.threeNumDocs, self.vocab, self.threeWordCount)
        classPs["four"], wordConditionalPs["four"] = calcP(self.totalDocs, self.fourNumDocs, self.vocab, self.fourWordCount)
        classPs["five"], wordConditionalPs["five"] = calcP(self.totalDocs, self.fiveNumDocs, self.vocab, self.fiveWordCount)
        return classPs, wordConditionalPs, len(self.vocab)
    
    def testNaiveBayes(self, test, classPs, wordConditionalPs, vocabSize):
        oneVal = 0
        twoVal = 0
        threeVal = 0
        fourVal = 0
        fiveVal = 0
        # READ THE TEST REVIEW
        testText = test[1]
        # PREPROOCESS DOC STRING
        testTokens = self.preprocess(testText)
        # MULTIPLY PROBABILITIES FOR EACH CATEGORY
        for tok in testTokens:
            if tok in wordConditionalPs["one"]:
                oneVal += math.log10(wordConditionalPs["one"][tok])
            else: # account for if word in test review is not in training data set
                oneVal += math.log10(1 / (len(wordConditionalPs["one"]) + vocabSize))
            if tok in wordConditionalPs["two"]:
                twoVal += math.log10(wordConditionalPs["two"][tok])
            else: # account for if word in test review is not in training data set
                twoVal += math.log10(1 / (len(wordConditionalPs["two"]) + vocabSize))
            if tok in wordConditionalPs["three"]:
                threeVal += math.log10(wordConditionalPs["three"][tok])
            else: # account for if word in test review is not in training data set
                threeVal += math.log10(1 / (len(wordConditionalPs["three"]) + vocabSize))
            if tok in wordConditionalPs["four"]:
                fourVal += math.log10(wordConditionalPs["four"][tok])
            else: # account for if word in test review is not in training data set
                fourVal += math.log10(1 / (len(wordConditionalPs["four"]) + vocabSize))
            if tok in wordConditionalPs["five"]:
                fiveVal += math.log10(wordConditionalPs["five"][tok])
            else: # account for if word in test review is not in training data set
                fiveVal += math.log10(1 / (len(wordConditionalPs["five"]) + vocabSize))
        oneVal += math.log10(classPs["one"])
        twoVal += math.log10(classPs["two"])
        threeVal += math.log10(classPs["three"])
        fourVal += math.log10(classPs["four"])
        fiveVal += math.log10(classPs["five"])
        maxVal = max(oneVal, twoVal, threeVal, fourVal, fiveVal)
        # return the predicted class
        if maxVal == oneVal:
            return "1"
        elif maxVal == twoVal:
            return "2"
        elif maxVal == threeVal:
            return "3"
        elif maxVal == fourVal:
            return "4"
        elif maxVal == fiveVal:
            return "5"


""" END OF NAIVE BAYES CLASS"""


# HELPER FUNCTION that calculates probabilities needed for testing  ----------------------------------------------
def calcP(totalDocs, numCategoryDocs, vocab, wordCountDict):
    # dictionary of word conditional probabilities, {word1: P1, word2: P2, word3: P3, ...}
    conditionalP = {}
    # calculate class probability
    classP = numCategoryDocs / totalDocs
    # for every word in vocabulary of entire training set
    for word in vocab:
        # account for if word in vocab is not found in text for the category
        if word in wordCountDict:
            conditionalP[word] = (wordCountDict[word] + 1) / (len(wordCountDict) + len(vocab))
        else:
            conditionalP[word] = 1 / (len(wordCountDict) + len(vocab))
    return classP, conditionalP

