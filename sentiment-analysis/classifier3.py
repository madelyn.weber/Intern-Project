# python3 sentiment-analysis/classifier3.py
import h2o
from h2o.automl import H2OAutoML
import pandas as pd
from sklearn.model_selection import train_test_split
import nltk
from nltk.stem.snowball import SnowballStemmer
import re
from sklearn.feature_extraction.text import TfidfVectorizer

# initialize H2O and runs H2O clusters
h2o.init()
# install the library from NLTK that will perform tokenization
nltk.download('punkt')
nltk.download('stopwords')
 # initialize stemming algorithm
stemmer = nltk.stem.SnowballStemmer('english')
stop_words = set(nltk.corpus.stopwords.words('english'))

""" FUNCTION THAT tokenizes the text, removes the unnecessary characters, transforms the text to lower case, and performs stemming
    RETURNS: stemmed words"""
def preprocessing(text):
    tokens = [word for word in nltk.word_tokenize(text) if (len(word) > 3 and len(word.strip('Xx/')) > 2 and len(re.sub('\d+', '', word.strip('Xx/'))) > 3) ] 
    tokens = map(str.lower, tokens)
    stems = [stemmer.stem(item) for item in tokens if (item not in stop_words)]
    return stems

def main():

    df = pd.read_csv('./sentiment-analysis/cleaned_reviews.csv')
    print(df['Rating'].value_counts())
    # create dictionary object that encodes the Ratings classes as integer/numeric values. 
    # The integer values representing the Rating classes will be between 1 to 5
    target = {'1': 1, '2': 2, '3': 3, '4': 4, '5': 5}
    # df['target']=df['Rating'].map(target)
    # df['target']=df.index.map(lambda x: target[x])
    df['target'] = df.index.to_series().map(target)
    print(df)
    # split the customer complaints dataset into two sets: model training and model testing
    X_train, X_test = train_test_split(df, test_size=0.2, random_state=111)
    vectorizer_tf = TfidfVectorizer(tokenizer=preprocessing, stop_words=None, max_df=1, max_features=1000, lowercase=False, ngram_range=(1,3))

    train_vectors = vectorizer_tf.fit_transform(X_train.ReviewText) 
    test_vectors = vectorizer_tf.transform(X_test.ReviewText)

    # converts the train-set and test-set into an array and adds the 1000 features selected from the original text data
    train_df = pd.DataFrame(train_vectors.toarray(), columns=vectorizer_tf.get_feature_names_out())
    test_df = pd.DataFrame(test_vectors.toarray(), columns=vectorizer_tf.get_feature_names_out())  
    # add the target column
    # final data frames will have the 1000 features and the target column
    train_df = pd.concat([train_df,X_train['target'].reset_index(drop=True)], axis=1)
    test_df = pd.concat([test_df,X_test['target'].reset_index(drop=True)], axis=1) 
    # convert Pandas Data Frame to H2O Data Frame. The H2O will use the created Data Frame during algorithm selection and training
    h2o_train_df = h2o.H2OFrame(train_df)
    h2o_test_df = h2o.H2OFrame(test_df)
    h2o_train_df['target'] = h2o_train_df['target'].asfactor()
    h2o_test_df['target'] = h2o_test_df['target'].asfactor()
    
    # initialize the H2OAutoML with the following parameters
    """
    MAX_MODELS: maximum number of models that H2OAutoML will run
    SEED: to ensure model reproducibility
    EXCLUDE_ALGOS: specifies that the algorithms H2OAutoML should not use during model training. H2OAutoML will skip the StackedEnsemble algorithms.
    BALANCE_CLASSES: handleS the imbalanced dataset. set to true to balance the five classes
    NFOLDS: specifies the number of k-fold cross-validation of the H2OAutoML model
    MAX_AFTER_BALANCE_SIZE: specifies the maximum relative size of the training data after balancing the classes
    """
    aml = H2OAutoML(max_models = 5, seed = 10, exclude_algos = ["StackedEnsemble"], verbosity="info", nfolds=0, balance_classes=True, max_after_balance_size=0.3)
    # x variable contains all the input features during training. 
    # The y variable contains the output/target column
    x = vectorizer_tf.get_feature_names_out().tolist()
    y = 'target'
    aml.train(x = x, y = y, training_frame = h2o_train_df, validation_frame=h2o_test_df)
    # aml.leaderboard
    # # use the best model to predict the test data frame(h2o_test_df)
    # pred=aml.leader.predict(h2o_test_df)
    # print(pred)

    exit(0)

if __name__ == "__main__":
    main()