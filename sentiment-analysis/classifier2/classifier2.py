# python3 sentiment-analysis/classifier2.py
from cgi import test
import os
# from test_rocchio import test_rocchio
# from train_rocchio import train_rocchio
from csv import reader
from naivebayes import *
from sklearn.metrics import confusion_matrix
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns

DATA_FILENAME = './sentiment-analysis/data/all_cleaned_reviews.csv'

def main():
    naiveBayesClassifier()

def naiveBayesClassifier():
    categories = {
        '1': {"recalled": 0, "relevant": 0, "correct": 0},
        '2': {"recalled": 0, "relevant": 0, "correct": 0},
        '3': {"recalled": 0, "relevant": 0, "correct": 0},
        '4': {"recalled": 0, "relevant": 0, "correct": 0},
        '5': {"recalled": 0, "relevant": 0, "correct": 0},
    }
    numCorrect = 0
    totalReviews = 0
    firstIteration = True
    # skip first line i.e. read header first and then iterate over each row od csv as a list
    with open(DATA_FILENAME, 'r') as f:
        csv_reader = reader(f)
        # NOTE: May not be the most efficient for memory, the below code stores the entire csv as a 2D list
        data = list(csv_reader)
    totalReviews = len(data)
    # remove the first row which consists of the column headers
    data.pop(0)
    predList = [None]* len(data)
    truthList = [None]* len(data)


    with open("./sentiment-analysis/classifier2.output.txt", 'w') as f:
        # Create Training Set through Cross-validation: For all N reviews, train (on the other N - 1 reviews)
        # and test Naive Bayes classifier on the remaining review:
        # Iterate over each row after the header in the csv
        for index, row in enumerate(data):
            trainingSet = []
            # row variable is a list that represents a row in csv
            for count, review in enumerate(data):
                # leave one review for testing (N-Cross Fold)
                if count != index:
                    trainingSet.append(review)
            nb = NaiveBayes()
            classPs, wordConditionalPs, vocabSize = nb.trainNaiveBayes(trainingSet)
            # print out the top 10 conditional probabilities
            if firstIteration:
                # sort the One Star class conditional probabilities in descending order
                f.write("One Star\n")
                sorted = Counter(wordConditionalPs["one"])
                output = dict(sorted.most_common(10))
                for word in output:
                    f.write(word + " " + str(wordConditionalPs["one"][word]) + "\n")
                # sort the One Star class conditional probabilities in descending order
                f.write("Two Star\n")
                sorted = Counter(wordConditionalPs["two"])
                output = dict(sorted.most_common(10))
                for word in output:
                    f.write(word + " " + str(wordConditionalPs["two"][word]) + "\n")
                # sort the One Star class conditional probabilities in descending order
                f.write("Three Star\n")
                sorted = Counter(wordConditionalPs["three"])
                output = dict(sorted.most_common(10))
                for word in output:
                    f.write(word + " " + str(wordConditionalPs["three"][word]) + "\n")
                # sort the One Star class conditional probabilities in descending order
                f.write("Four Star\n")
                sorted = Counter(wordConditionalPs["four"])
                output = dict(sorted.most_common(10))
                for word in output:
                    f.write(word + " " + str(wordConditionalPs["four"][word]) + "\n")
                # sort the One Star class conditional probabilities in descending order
                f.write("Five Star\n")
                sorted = Counter(wordConditionalPs["five"])
                output = dict(sorted.most_common(10))
                for word in output:
                    f.write(word + " " + str(wordConditionalPs["five"][word]) + "\n")
                firstIteration = False
            # test Naive Bayes on the remaining file
            prediction = nb.testNaiveBayes(row, classPs, wordConditionalPs, vocabSize)
            correctRating = row[0]
            f.write(str(correctRating) + " " + prediction + "\n")
            predList[index] = prediction
            truthList[index] = correctRating
            if prediction == correctRating:
                numCorrect += 1
                categories[prediction]["correct"] += 1
            # Calculate Precision and Recall
            categories[prediction]["recalled"] += 1
            categories[correctRating]["relevant"] += 1
                        
        f.write("Accuracy: " + str(numCorrect / totalReviews))
        conf_mat = confusion_matrix(predList, truthList)
        fig, ax = plt.subplots(figsize=(10,10))
        sns.heatmap(conf_mat, annot=True, fmt='d',
                    xticklabels=[1, 2, 3, 4, 5], yticklabels=[1, 2, 3, 4, 5])
        plt.ylabel('Actual')
        plt.xlabel('Predicted')
        plt.show()

        for cat in categories:
            f.write("----------", cat, "----------")
            if categories[cat]["relevant"] == 0:
                f.write("recall:\t\t0.0")
            else:
                f.write("recall:\t\t", float(categories[cat]["recalled"])/categories[cat]["relevant"])
            if categories[cat]["recalled"] == 0:
                f.write("precision:\t0.0")
            else:
                f.write("precision:\t", float(categories[cat]["correct"])/categories[cat]["recalled"])

if __name__ == "__main__":
    main()
