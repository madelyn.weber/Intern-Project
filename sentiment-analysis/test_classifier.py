from matplotlib import testing
from sklearn.naive_bayes import MultinomialNB
from sklearn.feature_extraction import DictVectorizer
from sklearn import svm
from collections import Counter
import random
import nltk

class classifier:

    def __init__(self):
        self.class_list = []    # list to hole the class associated with each piece of text for training
        self.feats_list = []    # list to hold the text associated with each piece of text for training
        self.test_class_list = []   # list to hold the class associated with each piece of text for testing
        self.test_feats_list = []   # list to hold the text associated with each piece of text for testing
        self.assigned_classifications = []  # list to hold the classifier's predicited classifications
        self.stopwords = ["a", "the", "to", "i", "and", "an", "of", "for", "be", "got", "they", "them", "he", "she", 
                            "him", "her", "was", "that", "my", "it", "with", "we", "in", "on", "me", "this", "had", "in", "so",
                            "our", "were", "have", "did", "but", "would", "did", "as", "when", "is", "do", "if", "at", "you"]
        # for testing
        self.class_1_uni = []
        self.class_1_bi = []
        self.class_1_tri = []
        self.class_2_uni = []
        self.class_2_bi = []
        self.class_2_tri = []
        self.class_3_uni = []
        self.class_3_bi = []
        self.class_3_tri = []
        self.class_4_uni = []
        self.class_4_bi = []
        self.class_4_tri = []
        self.class_5_uni = []
        self.class_5_bi = []
        self.class_5_tri = []

    def read_data(self, dataFile):
        data = open(dataFile).readlines()

        # removing column name line from dataset
        data = data[1:]

        # randomly split data into testing and training sets
        random.shuffle(data)
        training_data = data[:int((len(data)+1)*.8)]
        testing_data = data[int((len(data)+1)*.8):]

        # testing to make sure that testing and training data have no overlapping elements
        set_a = set(training_data)
        set_b = set(testing_data)
        if list(set_a.intersection(set_b)) != []:
            print("\n WARNING: check for data overlaps.")
            print(list(set_a.intersection(set_b)))
            print(len(list(set_a.intersection(set_b))))

        for line in training_data:
            split_line = line.split(',')
            class_ = split_line[0]
            text = split_line[1].strip('\n')
            # adding beginning and end of sentence markers
            text = "# " + text + " #"

            # print("Before stopword removal: ", text)
            text = self.remove_stopwords(text)
            # print("After stopword removal: ", text)

            # get n-grams of each sentence
            self.get_ngrams(text, False)
            # append class_ to a list of classifiers
            self.class_list.append(class_)

        for line in testing_data:
            split_line = line.split(',')
            class_ = split_line[0]
            text = split_line[1].strip('\n')
            # adding beginning and end of sentence markers
            text = "# " + text + " #"

            # print("Before stopword removal: ", text)
            text = self.remove_stopwords(text)
            # print("After stopword removal: ", text)

            # get n-grams of each sentence
            self.get_ngrams(text, True)
            # append class_ to a list of classifiers
            self.test_class_list.append(class_)

            # for testing n-grams
            self.test_ngrams(class_, text)

    def remove_stopwords(self, text):
        hold_words = []
        for word in text.split(" "):
            if word not in self.stopwords:
                hold_words.append(word)
        return " ".join(hold_words)

    # gets ngrams from text
    def get_ngrams(self, text, isTestData):

        ngrams_list = []
        # get unigrams
        unigrams = nltk.ngrams(text.split(), 1)
        ngrams_list.extend(list(unigrams))

        # get bigrams
        bigrams = nltk.ngrams(text.split(), 2)
        ngrams_list.extend(list(bigrams))

        # get trigrams
        trigrams = nltk.ngrams(text.split(), 3)
        ngrams_list.extend(list(trigrams))

        # place all three ngrams plus occurence counts in dictionary
        ngram_dict = {}
        for ngram in ngrams_list:
            if ngram not in ngram_dict:
                ngram_dict[ngram] = 1
            else:
                ngram_dict[ngram] += 1

        if isTestData:
            self.test_feats_list.append(ngram_dict)
        else:
            self.feats_list.append(ngram_dict)

    def classify(self):
        vectorizer = DictVectorizer(sparse=True)
        features = vectorizer.fit_transform(self.feats_list)
        classes = self.class_list
        linearSvc = svm.LinearSVC()
        linearSvc.fit(features, classes)

        # running classifier on testing dataset now
        self.assigned_classifications = linearSvc.predict(vectorizer.transform(self.test_feats_list))

    def test_ngrams(self, class_, text):

        # get unigrams
        unigrams = nltk.ngrams(text.split(), 1)

        # get bigrams
        bigrams = nltk.ngrams(text.split(), 2)

        # get trigrams
        trigrams = nltk.ngrams(text.split(), 3)

        if class_ == "1":
            self.class_1_uni.extend(list(unigrams))
            self.class_1_bi.extend(list(bigrams))
            self.class_1_tri.extend(list(trigrams))
        elif class_ == "2":
            self.class_2_uni.extend(list(unigrams))
            self.class_2_bi.extend(list(bigrams))
            self.class_2_tri.extend(list(trigrams))
        elif class_ == "3":
            self.class_3_uni.extend(list(unigrams))
            self.class_3_bi.extend(list(bigrams))
            self.class_3_tri.extend(list(trigrams))
        elif class_ == "4":
            self.class_4_uni.extend(list(unigrams))
            self.class_4_bi.extend(list(bigrams))
            self.class_4_tri.extend(list(trigrams))
        else:
            self.class_5_uni.extend(list(unigrams))
            self.class_5_bi.extend(list(bigrams))
            self.class_5_tri.extend(list(trigrams))


def main():
    classify = classifier()
    classify.read_data("../webscraper_test/data/total_data_collected_cleaned.csv")
    print(len(classify.feats_list))
    print(len(classify.class_list))
    classify.classify()

    is_correct = 0
    total = 0
    try:
        if len(classify.assigned_classifications) == len(classify.test_class_list):
            for idx, item in enumerate(classify.assigned_classifications):
                if classify.test_class_list[idx] == item:
                    is_correct += 1
                total += 1
    except:
        print("An error has occured.")

    print("\nThis classifier's accuracy is: {}.".format(is_correct / total))

    print()
    print("class 1 list: \n")
    print(Counter(classify.class_1_uni).most_common(6), "\n", Counter(classify.class_1_bi).most_common(5), "\n", Counter(classify.class_1_tri).most_common(5))
    print()
    print("class 2 list: \n")
    print(Counter(classify.class_2_uni).most_common(6), "\n", Counter(classify.class_2_bi).most_common(5), "\n", Counter(classify.class_2_tri).most_common(5))
    print()
    print("class 3 list: \n")
    print(Counter(classify.class_3_uni).most_common(6), "\n", Counter(classify.class_3_bi).most_common(5), "\n", Counter(classify.class_3_tri).most_common(5))
    print()
    print("class 4 list: \n")
    print(Counter(classify.class_4_uni).most_common(6), "\n", Counter(classify.class_4_bi).most_common(5), "\n", Counter(classify.class_4_tri).most_common(5))
    print()
    print("class 5 list: \n")
    print(Counter(classify.class_5_uni).most_common(6), "\n", Counter(classify.class_5_bi).most_common(5), "\n", Counter(classify.class_5_tri).most_common(5))
    print()

    # TODO: calculate a confusion matrix on data for accuracy

    

if __name__ == "__main__":
    main()
